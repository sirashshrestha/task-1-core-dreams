@extends('layout.app')
@section('title')
    <title>Information Stored in CSV</title>
@endsection
@section('content')
    <div class="container">
        <hr/>
        <a href="{!! route('task.information.form') !!}" class="btn btn-primary">Add Information To CSV</a>
        <hr/>
        <div class="row">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Gender</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Nationality</th>
                    <th>Date of Birth</th>
                    <th>Education Background</th>
                    <th>Preferred Contact Mode</th>
                </tr>
                </thead>
                <tbody>
                @if($information)
                    @foreach($information as $key=>$informationData)
                        <tr>
                            <td>{!! $informationData['name'] !!}</td>
                            <td>{!! $informationData['gender'] !!}</td>
                            <td>{!! $informationData['phone'] !!}</td>
                            <td>{!! $informationData['email'] !!}</td>
                            <td>{!! $informationData['address'] !!}</td>
                            <td>{!! $informationData['nationality'] !!}</td>
                            <td>{!! $informationData['dob'] !!}</td>
                            <td>{!! $informationData['education_background'] !!}</td>
                            <td>{!! $informationData['preferred_contact_mode'] !!}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <b>No Information Found in CSV file</b>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@endsection