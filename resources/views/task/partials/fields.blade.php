<div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
    {{ Form::label('name', 'Name: ') }}
    {{ Form::text('name', Request::old('name'), ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
</div>
@if ($errors->has('name'))
    <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
    </span>
@endif

<div class="form-group {{ $errors->has('gender') ? ' has-error' : '' }}">
    {{ Form::label('gender', 'Gender: ') }}
    <br/>
    &nbsp;{{ Form::radio('gender', 'Female', true) }}&nbsp;
    &nbsp;<span>Female</span>&nbsp;
    &nbsp;{{ Form::radio('gender', 'Male') }}&nbsp;
    &nbsp;<span>Male</span>&nbsp;
    &nbsp;{{ Form::radio('gender', 'Other') }}&nbsp;
    &nbsp;<span>Other</span>&nbsp;
</div>
@if ($errors->has('gender'))
    <span class="help-block">
        <strong>{{ $errors->first('gender') }}</strong>
    </span>
@endif

<div class="form-group {{ $errors->has('phone') ? ' has-error' : '' }}">
    {{ Form::label('phone', 'Phone: ') }}
    {{ Form::text('phone', Request::old('phone'), ['class' => 'form-control', 'placeholder' => 'Enter Phone Number']) }}
</div>
@if ($errors->has('phone'))
    <span class="help-block">
        <strong>{{ $errors->first('phone') }}</strong>
    </span>
@endif

<div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
    {{ Form::label('email', 'Email: ') }}
    {{ Form::email('email', Request::old('email'), ['class' => 'form-control', 'placeholder' => 'Enter Email']) }}
</div>
@if ($errors->has('email'))
    <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
    </span>
@endif

<div class="form-group {{ $errors->has('address') ? ' has-error' : '' }}">
    {{ Form::label('address', 'Address: ') }}
    {{ Form::text('address', Request::old('address'), ['class' => 'form-control', 'placeholder' => 'Enter Address']) }}
</div>
@if ($errors->has('address'))
    <span class="help-block">
        <strong>{{ $errors->first('address') }}</strong>
    </span>
@endif

<div class="form-group {{ $errors->has('nationality') ? ' has-error' : '' }}">
    {{ Form::label('nationality', 'Nationality: ') }}
    {{ Form::text('nationality', Request::old('nationality'), ['class' => 'form-control', 'placeholder' => 'Enter Nationality']) }}
</div>
@if ($errors->has('nationality'))
    <span class="help-block">
        <strong>{{ $errors->first('nationality') }}</strong>
    </span>
@endif
<div class="form-group  {{ $errors->has('dob') ? ' has-error' : '' }}">
    <label>Date of Birth:</label>

    <div class="input-group date">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
        <input type="text" name="dob" class="form-control pull-right" id="datepicker">
    </div>
    <!-- /.input group -->
</div>

<div class="form-group {{ $errors->has('education_background') ? ' has-error' : '' }}">
    {{ Form::label('education_background', 'Education Background: ') }}
    <br/>
    {!! Form::select('education_background', [
        'SLC' => 'SLC',
        'Intermediate' => 'Intermediate',
        'Graduate' => 'Graduate',
        'Post-Graduate' => 'Post-Graduate',
        'PHD' => 'PHD'
    ], null)!!}
</div>
@if ($errors->has('education_background'))
    <span class="help-block">
        <strong>{{ $errors->first('education_background') }}</strong>
    </span>
@endif

<div class="form-group {{ $errors->has('preferred_contact_mode') ? ' has-error' : '' }}">
    {{ Form::label('preferred_contact_mode', 'Preferred Contact Mode: ') }}
    <br/>
    &nbsp;{{ Form::radio('preferred_contact_mode', 'Phone', true) }}&nbsp;
    &nbsp;<span>Phone</span>&nbsp;
    &nbsp;{{ Form::radio('preferred_contact_mode', 'Email') }}&nbsp;
    &nbsp;<span>Email</span>&nbsp;
    &nbsp;{{ Form::radio('preferred_contact_mode', 'None') }}&nbsp;
    &nbsp;<span>None</span>&nbsp;
</div>



