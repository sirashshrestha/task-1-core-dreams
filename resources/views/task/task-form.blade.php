@extends('layout.app')
@section('title')
    <title>Basic Information Form</title>
@endsection
@section('css')
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="{{ asset('assets/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
    <div class="container">
        <hr/>
        <a href="{!! route('task.information.index') !!}" class="btn btn-primary">View Information in CSV</a>
        <hr/>
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                {{ Form::open(['route' => 'task.information.store', 'id' =>'basic-information-form']) }}
                {{ csrf_field() }}
                <div class="box-body">
                    @include('task.partials.fields')
                    <div class="box-footer">
                        {{--<a onclick="addForm()" class="btn btn-primary">Add More</a>--}}
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <button type="reset" class="btn btn-warning">Reset</button>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('assets/dist/js/jquery.validate.min.js') }}"></script>
    <!-- bootstrap datepicker -->
    <script src="{{ asset('assets/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <!-- Select2 -->
    <script src="{{ asset('assets/dist/js/select2.full.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#basic-information-form').validate({
                rules: {
                    name: {
                        required: true
                    },
                    gender: {
                        required: true
                    },
                    phone: {
                        required: true
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    address: {
                        required: true
                    },
                    nationality: {
                        required: true,
                    },
                    dob: {
                        required: true,
                        date: true
                    },
                    education_background: {
                        required: true,
                    },
                    preferred_contact_mode: {
                        required: true,
                    },
                }
            });
        });
        $(function () {
            //Date picker
            $('#datepicker').datepicker({
                autoclose: true
            })
        })
    </script>
@endsection