<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('title')
    <link rel="stylesheet" href="{{ asset('assets/dist/css/bootstrap.min.css') }}">
    @yield('css')
</head>
<body>
<div class="container">
    <div class="session-message">
        @if(\Session::has('message'))
            <p class="alert {{ \Session::get('alert-class', 'alert-info') }}">{{ \Session::get('message') }}
                <span class="close-button btn btn-danger pull-right">X</span>
            </p>
        @endif
    </div>
    @yield('content')
</div>
<!-- ./wrapper -->
<script src="{{ asset('assets/dist/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/bootstrap.min.js') }}"></script>
@yield('scripts')
<script>
    $(document).ready(function () {
        $(".close-button").click(function () {
            $(".session-message").toggleClass("hidden");
        });
    });
</script>
</body>
</html>