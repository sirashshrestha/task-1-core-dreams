<?php namespace App\Application\Repositories\Task;

use Maatwebsite\Excel\Facades\Excel;

/**
 * Class TaskRepository
 * @package App\Application\Repositories\Task
 */
class TaskRepository implements TaskInterface
{
    /**
     * TaskRepository constructor.
     */
    public function __construct()
    {
    }

    /**
     * @param $request
     * @return bool|null
     */
    public function storeTaskInformationToCSV($request)
    {
        $filePath = base_path() . '\public\assets\data.csv';
        if (!file_exists($filePath)) {
            $csvData = array('Name, Gender, Phone, Email, Address, Nationality, DOB, Education Background,Preferred Contact Mode');
        }
        $csvData[] = $request['name']
            . ',' . $request['gender']
            . ',' . $request['phone']
            . ',' . $request['email']
            . ',' . $request['address']
            . ',' . $request['nationality']
            . ',' . $request['dob']
            . ',' . $request['education_background']
            . ',' . $request['preferred_contact_mode'];

        try {
            $file = fopen($filePath, "a");
            foreach ($csvData as $expData) {
                fputcsv($file, explode(',', $expData));
            }

            fclose($file);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return array|null
     */
    public function getTaskInformationFromCSV()
    {
        $filePath = base_path() . '\public\assets\data.csv';
        if (file_exists($filePath)) {
            $data = Excel::load(
                $filePath,
                function ($reader) {
                }
            )->get();

            $dataImported = null;
            if (!empty($data) && $data->count()) {
                $data = $data->toArray();
                for ($i = 0; $i < count($data); $i ++) {
                    $dataImported[] = $data[$i];
                }
            }

            return $dataImported;
        }

        return null;
    }
}