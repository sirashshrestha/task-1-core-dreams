<?php namespace App\Application\Repositories\Task;

/**
 * Interface TaskInterface
 * @package App\Application\Repositories\Task
 */
interface TaskInterface
{
    /**
     * @param $request
     * @return mixed
     */
    public function storeTaskInformationToCSV($request);

    /**
     * @return mixed
     */
    public function getTaskInformationFromCSV();
}
