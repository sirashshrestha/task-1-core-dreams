<?php namespace App\Application\Services\Task;

use App\Application\Repositories\Task\TaskInterface;
use Exception;

/**
 * Class TaskService
 * @package App\Application\Services\Task
 */
class TaskService
{
    /**
     * @var TaskInterface
     */
    protected $taskInterface;

    /**
     * TaskService constructor.
     * @param TaskInterface $taskInterface
     */
    public function __construct(TaskInterface $taskInterface)
    {
        $this->taskInterface = $taskInterface;
    }

    /**
     * @param $request
     * @return mixed
     */
    public function storeTaskInformationToCSV($request)
    {
        try {
            return $this->taskInterface->storeTaskInformationToCSV($request);
        } catch (Exception $exception) {
            return $exception;
        }

    }

    public function getTaskInformationFromCSV()
    {
        return $this->taskInterface->getTaskInformationFromCSV();
    }


}
