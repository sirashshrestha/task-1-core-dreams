<?php

namespace App\Http\Controllers\Task;

use App\Application\Services\Task\TaskService;
use App\Http\Controllers\Controller;
use App\Http\Request\TaskRequest;

/**
 * Class TaskController
 * @package App\Http\Controllers\Task
 */
class TaskController extends Controller
{
    /**
     * @var TaskService
     */
    private $taskService;

    /**
     * TaskController constructor.
     * @param TaskService $taskService
     */
    public function __construct(TaskService $taskService)
    {
        $this->taskService = $taskService;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getTaskInformationForm()
    {
        return view('task.task-form');
    }

    /**
     * @param TaskRequest $taskRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeTaskInformationToCSV(TaskRequest $taskRequest)
    {
        if ($this->taskService->storeTaskInformationToCSV($taskRequest->all())) {
            \Session::flash('message', 'Information has been added!');
            \Session::flash('alert-class', 'alert-success');
        } else {
            \Session::flash('message', 'Failed to add the information!');
            \Session::flash('alert-class', 'alert-danger');
        }

        return redirect()->back();
    }

    public function getTaskInformationFromCSV()
    {
        $information = $this->taskService->getTaskInformationFromCSV();

        return view('task.information.index', compact('information'));
    }
}
