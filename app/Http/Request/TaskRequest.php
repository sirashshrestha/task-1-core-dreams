<?php

namespace App\Http\Request;

use Illuminate\Foundation\Http\FormRequest;

class TaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                   => 'required|max:255',
            'gender'                 => 'required',
            'phone'                  => 'required',
            'email'                  => 'required|email|max:255',
            'address'                => 'required',
            'nationality'            => 'required',
            'dob'                    => 'required',
            'education_background'   => 'required',
            'preferred_contact_mode' => 'required',
        ];
    }
}

